# HEXIFALL IMPLEMENT GUIDE

考慮到我已經寫了不少有關Hexifall的程式碼，我決定把一些重要的函式寫在這，所以您不用再寫一次。每一個函式都有用三種程式語言實作——Lua、Python、Rust——但是不會用到很高深的特殊技巧（因為我不會），所以應該是滿通用的。因為Lua列表的索引始於1，所以把原點的座標改為$(1, 1)$。

以下所有Lua、Python、Rust程式碼的授權條款都是the MIT License，方便您複製貼上。

請享用雜亂的程式碼。

## 取得第n層的所有座標

- 輸入：
    - `n`：整數，第幾層
    - `mw`：整數，盤的寬度
- 輸出：
    1. 整數列表，索引是$x$座標，對應的值是$y$座標

### Lua

```lua
---Return coordinates in the nth layer
---@param n integer Which layer
---@param mw integer The width of the matrix
---@return integer[]
local function get_nth_layer(n, mw)
  local result = {}  ---@type integer[]
  for i = 1, math.min(mw, 2 * n) do
    result[#result+1] = n - math.ceil(i / 2) + 1
  end
  return result
end
```

### Python

```python
def get_nth_layer(n: int, mw: int) -> List[int]:
    """
    Return coordinates in the nth layer
    """
    result = []
    for i in range(min(mw, (n + 1) * 2)):
        result.append(n - i // 2)
    return result
```

### Rust

```rust
use core::cmp::min;

// Return coordinates in the nth layer
fn get_nth_layer(n: u32, mw: u32) -> Vec<u32> {
    let mut result: Vec<u32> = Vec::new();
    for i in 0..min(mw, (n + 1) * 2) {
        result.push(n - i / 2);
    }
    result
}
```

## 取得PC Hexi的生成位置

- 輸入：
    - `hexi_face`：
        - `shape`：二維布林值陣列，PC Hexi的形狀
        - `lowest_position`：整數座標，`shape`中`true`最低的位置
        - `center_col`：整數，`shape`的橫向中心
    - `spawn_layer`：整數，PC Hexi中心的橫向位置
    - `spawn_layer`：整數，PC Hexi圖形最低處所在的層之ID
    - `matrix_width`：整數，盤的寬度
- 輸出：
    1. 整數座標，PC Hexi的生成位置

### Lua

```lua
---Return new position of PC Hexi.
---@param hexi_face PCHexiFace
---@param spawn_layer integer
---@param spawn_column integer
---@return vec2
local function get_pc_hexi_spawn_position(hexi_face, spawn_column, spawn_layer, matrix_width)
  local pos_x = spawn_column - hexi_face.center_col + 1  -- one-indexed
  local pos_y = nth_layer(spawn_layer, matrix_width)[pos_x + hexi_face.lowest_position[1] - 1] - hexi_face.lowest_position[2] + 1
  return {pos_x, pos_y}
end
```

### Python

```python
def get_pc_hexi_spawn_position(hexi_face, spawn_column: int, spawn_layer: int, matrix_width: int) -> (int, int):
	pos_x = spawn_column - hexi_face.center_col
    pos_y = get_nth_layer(spawn_layer, matrix_width)[pos_x + hexi_face.lowest_position[0]] - hexi_face.lowest_position[1]
    return (pos_x, pos_y)
```

### Rust

```rust
struct HexiFace {
	shape: Vec<Vec<bool>>,
    lowest_position: (u32, u32),
    center_col: u32
    // ...
}

fn get_pc_hexi_spawn_position(hexi_face: HexiFace, spawn_column: u32, spawn_layer: u32, matrix_width: u32) -> (u32, u32) {
	let pos_x = spawn_column - hexi_face.center_col;
    let pos_y = get_nth_layer(spawn_layer, matrix_width)
        [usize::try_from(pos_x + hexi_face.lowest_position.0).unwrap()] - hexi_face.lowest_position.1;
    (pos_x, pos_y)
}
```
