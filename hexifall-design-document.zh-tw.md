# HEXIFALL DESIGN DOCUMENT

Hexifall（或「Hexiesfall」）是「在有限的時間內，疊合六角形」的遊戲。玩家在見到新的物件，要在它失去控制之前，把它放到合適的位置。玩家也要考慮畫面上其它在自我移動的物件，以免干擾。

Hexifall是即時遊戲，因此玩家想出的解法可能不是最好的，而這正是此遊戲的重點——玩家要想出的是夠好的解法，而不是**最好**的解法。

## 術語

- **Hiex**（六角）—平頂的六邊形。如果不屬於任何Hexi，那麼位置貼齊六角格線
- **Hexi**（聚六角）—由數個Hiexes邊對邊組成，可以側向移動的物體

    - **Player-controlled Hexi**（受控的六角）—受玩家操控的Hexi
    - **Self-moving Hexi**（自動的六角）—不受玩家操控的Hexi

- **matrix**（盤）—六角網格。玩家放置Hexies，使其成為Hiexes的地方。玩家只能看到在第$[(h_M-[\frac{w}{2}]) \div 2]$層（含）以下的部分，其中$h_M$、$w$分別為matrix的高與寬

    - Hexies在matrix的位置可表為$(x, \frac{y}{2})$，其中$x$、$y$為整數
    - 不屬於仼何Hexi的Hiexes之位置則可用$(m, n)$來表示，其中$m$、$n$為整數
    - Matrix上的空位也用Hiexes的表示法來表示
    - 原點$O(0, 0)$為在matrix最左上角的六角空格；$(1, 0)$為在$O$右下方的六角空格；$(0, 1)$為在$O$的正下方六角空格，如圖：

      ![座標示意圖](figs/hexifall-doc-fig6.svg)

- ***n*th layer** (of matrix)（第*n*層）—在matrix上，符合$(x, [\frac{x}{2}]+n)$的位置，其中$x$、$n$為整數
- **Stablize**（穩定）—player-controlled hexies轉變成self-moving Hexi或Hiexes的過程
- **Lock**（鎖定）—player-controlled hexies或self-moving Hexies轉變成Hiexes的過程
- **Slide**（滑動）—self-moving Hexies向左下或右下移動的過程
- **Fall**（下落）—Hexi向下移動一格的過程
- **Move**（側移）—Player-controlled Hexies向正左或正右移動的過程
- **Rotate**（轉動）—Player-controlled Hexies改變面向與位置的過程
- **Hard drop**（墜落）—Player-controlled Hexies在一瞬間不斷下落，直到無法下落的過程。在墜落之後，此PC Hexi會轉變成Hiexes或SM Hexies
- **Soft drop**（高速下落）—Player-controlled Hexies應玩家要求，以高於正常的速率（20倍）下落的過程
- **Collapsing**（崩壞）—固定在Matrix上的Hiexes被移除，在其之上的Hiexes下移的過程
- $(x, y)$ **is valid**（$(x, y)$合法）—在matrix上，$(x, y)$沒有被Hiexes占據。如果$y$非整數，則改為檢查$(x,  [y])$的下半部、$(x, [x]+1)$的上半部沒有被Hiexes佔據

## Player-controlled Hexies的自主移動與操控

Player-controlled Hexi (PC Hexi)可以下移、轉動、側移，但不能滑動。此外，如果Player-controlled Hexies無法下落，會穩定成self-moving Hexies (SM Hexi)。只有PC Hexi的位置可以與盤的格線相錯（也就是：位置為$(x, \frac{y}{2})$，$y$為奇數）。

### 穩定

如前所述，在嘗試下落時，若PC Hexi被Hiexes、SM Hexi、matrix邊界擋住，因而無法下落，則應穩定成SM Hexi。不過，在穩定之前，有1秒的延遲，如果此PC Hexi在此期間成功進行轉動或側移，延遲立即重設。

如果PC Hexi在下落之後，在它全部的Hiexes $H(x, y)$中有Hiexes位置不合法，且所有的Hiexes的位置改為$(x, y - 0.5)$後，位置合法，那麼該PC Hexi所有Hiexes的位置改為$(x, y - 0.5)$。其後進行穏定。這情形通常只有在PC Hexi與格線相錯時才會發生。

### 下落

設player-controlled Hexi 全部的Hiexes $H(x, y)$，如果全部的$(x, y + 1)$合法（檢查時，假設$H$不存在），那麼所有$H$的位置會變成$(x, y + 1)$。

### 轉動

Hexies的轉動可以分為兩階段：形狀改變與改變位置。

形狀改變即是將Hexi的形狀以順或逆時針旋轉數次，並且Hexi的視覺位置沒有改變（人工定義，見附錄）。如果形狀改變時，所有Hiexes的位置在matrix上都沒有其它物件，轉動便宣告成功。

如果在完成第一階段之後，轉動還沒成功，就執行第二階段。在改變位置時，Hexi要從位移表（見附錄）中，依序嘗試位移。在每次位移之後，如果位移之後的位置合法，就把Hexi的位置改成該位置，並宣告轉動成功。

如果完成兩階段之後，轉動未成功，轉動便宣告失敗。

### 側移

側移是把Hexi向正左或正右移動的過程。Hexi在側移之後可能不會貼齊六角格線，而這正是Hexies下落時是一次下落半格的原故。側移的示意圖如下：

![側移的示意圖](figs/hexifall-doc-fig3.svg)

如果要把Hexi $H(x,y)$向右移動$t$格（如果要向左移動，$t \lt 0$），$H$的位置會變成$H(x+t,y- \frac{t}{2})$。

### 墜落

**墜落**是讓玩家快速放置Hexies的機制，不過它也可以讓玩家把新生成的Hexi放在舊的Hexi下方。PC Hexi在墜落時，不斷下落，無視SM Hexi，直接穿過它們。然而，有時這樣會讓PC Hexi處在不合法的位置（因為PC Hexi的Hiexes與SM Hexi的Hiexes重疊）。以下為遇到此情形的解法：

 1. 讓擋住PC Hexi的SM Hexi無法被穿過
 2. 再重新墜落一次

另外，如果墜落沒有一次就成功，在墜落之後，此PC Hexi要轉變成SM Hexi，否擇轉變成Hiexes。如果PC Hexi轉變成SM Hexi，且此SM Hexi無法下落，那麼立即鎖定該SM Hexi。

以下是有SM Hexi擋住PC Hexi的圖示：

1. 圖中的PC Hexi準備墜落

   ![圖中有PC、SM Hexi，前者在後者上方](figs/hexifall-doc-fig5-1.svg)

2. 因為PC Hexi的位置不合法…

   ![PC Hexi因為與SM Hexi重疊，位置不合法](figs/hexifall-doc-fig5-2.svg)

3. 所以把紅色的SM Hexi視為不可穿過，把白色的PC Hexi轉變成SM Hexi

   ![PC Hexi在SM Hexi上方](figs/hexifall-doc-fig5-3.svg)

以下是「一般」的情形：

1. 圖中白色的PC Hexi要墜落

   ![圖中有PC、SM Hexi，前者在後者上方](figs/hexifall-doc-fig4-1.svg)

2. 因為位置合法，所以白色的PC Hexi會在紅色SM Hexi的下方，轉變成Hiexes

   ![PC Hexi在SM Hexi的下方](figs/hexifall-doc-fig4-2.svg)

   順帶一提，上圖紅色的SM Hexi會向右滑動

## Self-moving Hexies的自主移動與鎖定

Self-moving Hexies有兩種移動——下落與滑動。其中，滑動又可分為右滑與左滑。依優先序，先嘗試下落，再嘗試右滑，最後再嘗試左滑。

以下分項說明皆認定$H(x, y)$為SM Hexi的所有Hiexes（$H(x, y)$是一組座標）。此外，在檢查$(x, y)$是否合法時，假設$H$不存在。以下的條件都沒有考慮到優先序，在實作時只要考慮到以粗體字形表示的條件即可。

### 下落

如果**所有的$(x, y + 1)$都合法**，那麼把所有$H$的位置都改成$(x, y + 1)$。

> ⚠**注意**：只要所有$H$下方的位置都合法，該self-moving hexi就應該下落。這代表圖中白色的SM Hexi會下落：
>
> ![有一個H的左下、右下有Hiex，但下方沒有](figs/hexifall-doc-fig1.svg)

### 右滑

如果有任一$(x, y + 1)$不合法，且**所有的$(x + 1, y)$都合法**，那麼把所有$H$的位置都改成$(x + 1, y)$。

> ⚠**注意**：所有$H$的右方都合法、下方都不合法時，就該右滑。這代表圖中白色的SM Hexi會右滑：
>
> ![有一個H只有下方有Hiex擋著](figs/hexifall-doc-fig2.svg)

### 左滑

如果有任一$(x, y + 1)$且有任一$(x + 1, y)$不合法，並且**所有的$(x - 1, y + 1)$都合法**，那麼把所有$H$的位置都改成$(x - 1, y + 1)$。

### 鎖定

如果此SM Hexi在移動**之後**，$(x, y + 1)$、$(x + 1, y)$、$(x - 1, y + 1)$三群位置都分別有任一個位置不合法，那麼**立即**鎖定該SM Hexi。

## 盤、PC Hexies的生成與遊戲規則

為了方便說明，先定義名詞：

- **visual height**, $v$（視高）—matrix上某區域含有的層數。對matrix本身而言，其值$v_M$為$h_M-[\frac{w}{2}]$，其中$h_M$為matrix的高、$w$為matrix的寬
- **top layer**, $T$（最頂層）—第一個寬度為盤寬的層，即第$[\frac{w}{2}]$層，其中$w$是matrix的寬度
- **bottom layer**, $Z$（最底層）—第$h_M$層，其中$h_M$為matrix的高度
- **ceiling layer**, $C$（最上層）—第一個可以被玩家看見的層，即第$[\frac{h_M-[\frac{w}{2}]}{2}]$層
- **buffer zone**, $B$（緩衝區）—在ceiling layer（不含）之上，top layer（含）之下的層
- **locking zone**, $K$（鎖定區）—在bottom layer之上（含），top layer（不含）之下的層
- **Gravity tick**（重力周期）—在Matrix上，SM Hexi更新位置一次、PC Hexi嘗試下落的時刻

### PC Hexi的生成

PC Hexi生成在盤上時，該PC Hexi在最下方的Hiex緊貼$C$上方，位於第$[\frac{h_M-[\frac{w}{2}]}{2}]-1$層，且其圖形横向置中（或向右些許偏移）。在生成之後，此PC Hexi應立即下落一格，到鎖定區內。如果無法下落，遊戲結束。

生成時，PC Hexi的面向應佔有最少横向空間，且多數Hiexes在圖形上方偏左，以下是詳細的步驟：

- 把圖形們由左至右，由上至下轉換成二進位數字
- 選擇數字最小的圖形

在Matrix上有至多一個PC Hexi。PC Hexi鎖定之後，在下一個gravity tick才需生成新的PC Hexi。

### 盤

**在檢查Hexi位置是否合法時，需將在$Z$以下的區域視為被占據的位置**。這樣，盤的底部才不會歪一邊。

盤有重力周期，控制Hexi下落的速度。在每次重力周期時，以下事件依序發生：

1. SM Hexi更新位置
2. PC Hexi下落

重力周期可能與PC Hexi的穩定不同步，以下是兩者交互的規則：

- 在PC Hexi穩定前的延遲中，如果有重力周期，略過該重力周期
- PC Hexi穩定完後，重力周期的倒數計時重設，在其之後的重力周期順延

此外，如果PC Hexi鎖定成SM Hexi，該SM Hexi立即自主移動一次。

重力周期發生時，如果有兩個SM Hexesi緊貼，在接觸面下方的SM Hexi先移動。如果有多個Hiex要移動到同位置而產生衝突，**下移**優先於**左滑**優先於**右滑**。如果兩個SM Hexies $G$和$H$有至少兩個接觸面，且在其中一個接觸面上$G$在$H$下方，同時在另一個接觸面上$G$在$H$上方，則$G$和$H$同時下落。圖示如下，兩個SM Hexi同時下落：

![兩個SM Hexi交纏，所以同時下落](figs/hexifall-doc-fig8.svg)

**崩壞**是指盤上某一個被Hiexes填滿的層擁有的Hiexes被消除，在此層之上的Hiexes下移的過程。崩壞與盤的重力周期並不同步，而是與PC Hexi的穩定同步。**只有PC Hexi穩定完時，才嘗試崩壞**。嘗試崩壞時，由上到下，檢查盤上的層是否被Hiexes填滿。如果是，清空此層，並將在此層之上的Hiexes下移一格。以下是圖示：

1. 圖中的藍色PC Hexi準備穩定

   ![盤中有Hiexes與準備要穩定的PC Hexi](figs/hexifall-doc-fig7-1.svg)

2. 圖中的藍色PC Hexi已穩定，標示的區域準備崩壞

   ![圖中有標示區域](figs/hexifall-doc-fig7-2.svg)

3. 消除標示的區域，並把上方的Hiexes下移

   ![標示的地方被消除，上方的Hiexes下移](figs/hexifall-doc-fig7-3.svg)

4. 下方被Hiexes填滿的層以同方式處理

   ![另一個滿的層被標示](figs/hexifall-doc-fig7-4.svg)

   ![同樣，標示的地方被消掉，上方的Hiexes下移一格](figs/hexifall-doc-fig7-5.svg)

### 遊戲結束

以下是遊戲結束的條件：

- 新的PC Hexi無法生成
- PC Hexi在生成之後，無法下落

## 優先序

前面提到的各種移動可能會有衝突，以下是各種動作的優先序：

1. PC Hexi的各種移動：
   1. 墜落
   2. 轉動
   3. 高速下落
   4. 下落
2. SM Hexi的自主移動
3. 崩壞

## 附錄一：轉動的形狀與位移表

（未完成）
